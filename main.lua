function love.load()
	require "Func/drawFunc"
	require "Func/mainFunc"
	Object = require "Libs/classic"
	Camera = require "Libs/camera"
	Timer = require "Libs/timer"
	require "Objects/boss"
	require "Objects/player"
	require "Objects/bullet"
	require "Objects/grunt"

	love.window.setTitle("ABH - By fridays18")
	screenx = 400
	screeny = 400
	love.window.setMode(screenx, screeny)

	listOfEnemies = {}
	listOfBullets = {}

	player = Player()
	boss = Boss()

	timer = 200

	camera = Camera(200,200)



	font = love.graphics.newFont(80)
	smlfont = love.graphics.newFont(20)

	bossEvent = false
	gruntEvent = true

	BULLET_p = love.graphics.newImage("Art/bullet_part.png")
	--part bullet
	psystem = love.graphics.newParticleSystem(BULLET_p, 32)
	psystem:setParticleLifetime(1, 2) -- Particles live at least 2s and at most 5s.
	psystem:setLinearAcceleration(-30,-30,30,30) -- Randomized movement towards the bottom of the screen.
	psystem:setColors(255, 255, 255, 255, 255, 255, 255, 0) -- Fade to black
	--part player
	psystem2 = love.graphics.newParticleSystem(BULLET_p, 32)
	psystem2:setParticleLifetime(1, 2) -- Particles live at least 2s and at most 5s.
	psystem2:setLinearAcceleration(-30,-30,30,30) -- Randomized movement towards the bottom of the screen.
	psystem2:setColors(255, 255, 255, 255, 255, 255, 255, 0) -- Fade to black
end

function love.update(dt)
	psystem:update(dt)
	psystem2:update(dt)
	Timer.update(dt)
	
	timer = timer - 1
	if timer <= 0 then
		if gruntEvent == true then
		table.insert(listOfEnemies, Enemy()) 
		end
		timer = 200
	end

	if bossEvent == true then
		boss:update(dt)
	end
	
	player:update(dt)
--bullet update
	for i,v in ipairs(listOfBullets) do
        v:update(dt)
		--remove from list if off screen
				if v.alive == false then
            --Remove it from the list
            table.remove(listOfBullets, i)
        end

		--if the bullet is active do collisions
		if v.active == true then
		BuP = detectCollision(v.x,player.x,v.y,player.y,10)
			if bossEvent == true then
		BuB = detectCollision(v.x,boss.x,v.y,boss.y,boss.r)
			end
		end

		--actual collisions

		--boss col
		if BuB == true then
			if v.type == "player" then
			BuB = false
			boss.health = boss.health - 1
			table.remove(listOfBullets, i)
			cameraShake(.5)
			end
		end
		--player col
		if BuP == true then
				BuP = false
			if v.type ~= "boss" then
				player.health = player.health - 1
			else
				player.health = player.health - boss.dmg
			end
			
				table.remove(listOfBullets, i)
				cameraShake(2)
			psystem2:emit(30)
			end
		
    end
	
	--enemy update
	for i,v in ipairs(listOfEnemies) do
        v:update(dt)
		if v.alive == false then
            --Remove it from the list
            table.remove(listOfEnemies, i)
        end
    end

	--collision
	for i,v in ipairs(listOfBullets) do
  for i2, v2 in ipairs(listOfEnemies) do
			BuEn = detectCollision(v.x,v2.x,v.y,v2.y,10)
			if BuEn == true then
				BuEn = false
				table.remove(listOfBullets, i)
				table.remove(listOfEnemies, i2)
				cameraShake(1)
				if v.type == "player" then
					player.health = player.health + 1
				end
			end
  end
		end
	

	
end

function love.draw()
	camera:attach()
	
	love.graphics.rectangle("line",1,1,399,399)
	healthDraw()

	player:draw()
	love.graphics.draw(psystem2, player.x,player.y)
	
	if bossEvent == true then
		boss:draw()
	end

	
    --bullet update
	for i,v in ipairs(listOfBullets) do
        v:draw()
		love.graphics.draw(psystem, v.x,v.y)
    end
	--enemy update
for i,v in ipairs(listOfEnemies) do
        v:draw()
    end


	camera:detach()
end