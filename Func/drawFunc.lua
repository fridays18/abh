--Color setting
function colorStart(r,g,b,a)
	local major, minor, revision, codename = love.getVersion()

	if major == 11 then
		a = a / 150
		r = r / 150
		g = g / 150
		b = b / 150
	end
	
love.graphics.push("all")
love.graphics.setColor(r,g,b,a)
end

function colorEnd()
love.graphics.setColor(1, 1, 1, 1)
love.graphics.pop()
end

function cameraShake(str)
	local strength = str
	shaking = true
	Timer.during(.1, function()
    camera:lookAt(200 + math.random(-strength,strength), 200 + math.random(-strength,strength))
end, function()
    -- reset camera position
    camera:lookAt(200,200)
			shaking = false
end)
end

function healthDraw()
	love.graphics.setFont(font)
	love.graphics.printf(player.health,0,screeny / 2,screenx,"center",0,fontSize,fontSize)
end