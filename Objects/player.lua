Player = Object:extend()

function Player:new()
	self.x = 172
  self.y = 30
  self.speed = 100
	self.r = 10
	self.d = 4
	self.health = 10
	self.canShoot = true
	self.CD = 50
end

function Player:update(dt)
	
	if self.canShoot == false then
		self.CD = self.CD - 1
		if self.CD <= 0 then
			self.CD = 50
			self.canShoot = true
		end
	end
	
    if love.keyboard.isDown("a") then
		if self.x > 10 then
        self.x = self.x - self.speed * dt
		end
		self.d = 1
    elseif love.keyboard.isDown("d") then
		if self.x < screenx - 20 then
        self.x = self.x + self.speed * dt
		end
		self.d = 2
		elseif love.keyboard.isDown("w") then
		if self.y > 10 then
        self.y = self.y - self.speed * dt
		self.d = 3
		end
		elseif love.keyboard.isDown("s") then
		if self.y < screeny - 10 then
        self.y = self.y + self.speed * dt
		self.d = 4
		end
    end
	if love.keyboard.isDown("lshift") then
		self.speed = 200
	end
end



function Player:draw()
    love.graphics.circle("fill",self.x,self.y,self.r)
end