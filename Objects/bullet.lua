Bullet = Object:extend()

function Bullet:new(x, y, path,type)
    self.r = 4
    self.x = x
    self.y = y
    self.speed = 300
		self.path = path
	self.alive = true
	self.active = false
	self.timer = 20
	self.type = type
end

function Bullet:update(dt)
	psystem:emit(1)
	if self.timer > -5 then
	self.timer = self.timer - 1
	end
	if self.timer <= 0 then
		self.active = true
	end
	
	if self.x > screenx or self.x < 0 or self.y > screeny or self.y < 0 then
		self.alive = false
	end
	
	if self.path == 4 then
    self.y = self.y + self.speed * dt
	elseif self.path == 1 then
		self.x = self.x - self.speed * dt
	elseif self.path == 2 then
		self.x = self.x + self.speed * dt
	elseif self.path == 3 then
    self.y = self.y - self.speed * dt
		end
end

function Bullet:draw()
  love.graphics.circle("fill", self.x, self.y,self.r)
end

