--Makes enemy a class
Boss = Object.extend(Object)

--calling enemy() plays this function
function Boss.new(self)
    self.x = math.random(10,screenx)
    self.y = math.random(10, screeny)
		self.r = 50
	--shoot direction
		self.d = 0
	--cooldown before next shot
		self.cd = math.random(10,50)
	--timer to cd to shoot
	self.timer = 100
	--State switch timer
	self.timerSwitch = 200
	--bounce
	self.bounce = math.random(0,1)
	self.speed = 200

	self.stage = 1
	--health
	self.health = math.random(100,500)
	--timer preset
	self.timerDef = 100
	--dmg
	self.dmg = 1
		
end

function Boss.update(self, dt)
	--timers,shooting,and states
	self.timer = self.timer - 1
	self.timerSwitch = self.timerSwitch - 1
	--state timer
	if self.timerSwitch <= 0 then
		--resetting
		self.speed = 200
		self.timerDef = 100
		self.dmg = 1
		self.timerSwitch = 200
		cameraShake(5)
		--rerolling
		self.stage = math.random(1,5)
		if self.stage == 2 then
			self.timerDef = 50
		end
		if self.stage == 3 then
			self.speed = 400
		end
		if self.stage == 4 then
			self.dmg = 2
		end
	end
	--bullet and d timer
	if self.timer <= 0 then
		self.d = math.random(1,4)
		if self.d == 3 then
		table.insert(listOfBullets, Bullet(self.x, self.y - 20,self.d,"boss"))
			table.insert(listOfBullets, Bullet(self.x - 20, self.y - 20,self.d,"boss"))
			table.insert(listOfBullets, Bullet(self.x + 20, self.y - 20,self.d,"boss"))
			if self.stage == 5 then
				table.insert(listOfBullets, Bullet(self.x - 40, self.y - 20,self.d,"boss"))
			table.insert(listOfBullets, Bullet(self.x + 40, self.y - 20,self.d,"boss"))
			end
		end
		if self.d == 4 then
		table.insert(listOfBullets, Bullet(self.x, self.y + 20,self.d,"boss"))
			table.insert(listOfBullets, Bullet(self.x - 20, self.y + 20,self.d,"boss"))
			table.insert(listOfBullets, Bullet(self.x + 20, self.y + 20,self.d,"boss"))
			if self.stage == 5 then
table.insert(listOfBullets, Bullet(self.x - 40, self.y + 20,self.d,"boss"))
			table.insert(listOfBullets, Bullet(self.x + 40, self.y + 20,self.d,"boss"))
			end
		end
		if self.d == 1 then
		table.insert(listOfBullets, Bullet(self.x - 20, self.y,self.d,"boss"))
			table.insert(listOfBullets, Bullet(self.x - 20, self.y + 20,self.d,"boss"))
			table.insert(listOfBullets, Bullet(self.x - 20, self.y - 20,self.d,"boss"))
			if self.stage == 5 then
table.insert(listOfBullets, Bullet(self.x - 20, self.y + 40,self.d,"boss"))
			table.insert(listOfBullets, Bullet(self.x - 20, self.y - 40,self.d,"boss"))
			end
		end
		if self.d == 2 then
		table.insert(listOfBullets, Bullet(self.x + 20, self.y,self.d,"boss"))
			table.insert(listOfBullets, Bullet(self.x + 20, self.y + 20,self.d,"boss"))
			table.insert(listOfBullets, Bullet(self.x + 20, self.y - 20,self.d,"boss"))
			if self.stage == 5 then
table.insert(listOfBullets, Bullet(self.x + 20, self.y + 40,self.d,"boss"))
			table.insert(listOfBullets, Bullet(self.x + 20, self.y - 40,self.d,"boss"))
			end
		end
		self.timer = self.timerDef
	end
	--movement
	if self.d == 1 then
		if self.x > self.r then
		self.x = self.x - self.speed * dt
		else
			self.d = math.random(1,4)
		end
	end
	if self.d == 2 then
		if self.x < screenx - self.r then
		self.x = self.x + self.speed * dt
			else
			self.d = math.random(1,4)
		end
	end
	if self.d == 3 then
		if self.y > self.r then
		self.y = self.y - self.speed * dt
			else
			self.d = math.random(1,4)
		end
	end
	if self.d == 4 then
		if self.y < screeny - self.r then
		self.y = self.y + self.speed * dt
			else
			self.d = math.random(1,4)
		end
	end

	
end

function Boss.draw(self)
	love.graphics.setFont(smlfont)
	love.graphics.printf(self.health,self.x - 25,self.y - 10,self.r,"center")

	if self.stage == 1 then
		--white
		colorStart(255,255,255,255)
	end
	if self.stage == 2 then
		--yellow
		colorStart(255,255,0,255)
	end
	if self.stage == 3 then
		--red
		colorStart(255,0,0,255)
	end
	if self.stage == 4 then
		--purple
		colorStart(230,230,250,255)
	end
	if self.stage == 5 then
		--green
		colorStart(0,255,0,255)
	end
	love.graphics.circle("line",self.x,self.y,self.r)
	colorEnd()
end