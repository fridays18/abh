--Makes enemy a class
Enemy = Object.extend(Object)

--calling enemy() plays this function
function Enemy.new(self)
    self.x = math.random(10,screenx)
    self.y = math.random(10, screeny)
		self.r = 10
	--shoot direction
		self.d = 0
	--movement pattern
		self.pattern = 2
	--cooldown before next shot
		self.cd = math.random(10,50)
	--timer to cd to shoot
	self.timer = 200
	--bounce
	self.bounce = math.random(0,1)
	self.speed = math.random(100,200)
		
end

function Enemy.update(self, dt)
	--handling timer and shooting based off direction
	self.timer = self.timer - 1
	if self.timer <= self.cd then
		self.timer = 200
		if self.d == 3 then
		table.insert(listOfBullets, Bullet(self.x, self.y - 20,self.d,"grunt"))
		end
		if self.d == 4 then
		table.insert(listOfBullets, Bullet(self.x, self.y + 20,self.d,"grunt"))
		end
		if self.d == 1 then
		table.insert(listOfBullets, Bullet(self.x - 20, self.y,self.d,"grunt"))
		end
		if self.d == 2 then
		table.insert(listOfBullets, Bullet(self.x + 20, self.y,self.d,"grunt"))
		end
	end
	
	--move left to right
	if self.pattern == 1 then
		--setting bullet shooting
		if self.y > screeny / 2 then
			self.d = 3
		else
			self.d = 4
		end

		--bounce controls
		if self.bounce == 1 then
			 self.x = self.x + self.speed * dt
		else
			self.x = self.x - self.speed * dt
		end

		--bounce acknoledgement
		if self.x < 10 then
			self.bounce = 1
		end
		if self.x > screenx - 10 then
			self.bount = 0
		end

	end

		--move up to down
	if self.pattern == 2 then
		--setting bullet shooting
		if self.x > screenx / 2 then
			self.d = 1
		else
			self.d = 2
		end

		--bounce controls
		if self.bounce == 1 then
			 self.y = self.y + self.speed * dt
		else
			self.y = self.y - self.speed * dt
		end

		--bounce acknoledgement
		if self.y < 10 then
			self.bounce = 1
		end
		if self.y > screeny - 10 then
			self.bounce = 0
		end

	end
	
	
end

function Enemy.draw(self)
    love.graphics.circle("line",self.x,self.y,self.r)
end